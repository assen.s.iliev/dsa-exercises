﻿using System;
using System.Collections.Generic;

namespace AnagramWords
{
    class Program
    {
        static void Main(string[] args)
        {
            string word1 = "post";
            string word2 = "opt";

            word1 = word1.ToLower();
            word2 = word2.ToLower();

            HashSet<char> word1Set = new HashSet<char>(word1);

            bool contain = true;
            

            foreach (char ch in word2)
            {
                if (!word1Set.Contains(ch))
                {
                    contain = false;
                    break;
                }
            }

            if (contain && word1.Length == word2.Length) 
                Console.WriteLine($"{word1} and {word2} are anagrams");
            else
                Console.WriteLine($"{word1} and {word2} are not anagrams");
        }
    }
}
