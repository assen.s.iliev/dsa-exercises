﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DSA_Practice
{
    //Remove duplicate characters from string

    class Program
    {
        static void Main(string[] args)
        {
            string input = Console.ReadLine();
            HashSet<char> inputSet = new HashSet<char>();

            foreach (var ch in input)
            {
                inputSet.Add(ch);
            }

            string output = new string(inputSet.ToArray());
            Console.WriteLine(output);
        }
    }
}
